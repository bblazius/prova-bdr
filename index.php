<?php

require '../Slim/Slim/Slim.php';
\Slim\Slim::registerAutoloader();
$app = new \Slim\Slim();
$app->response()->header('Content-Type', 'application/json;charset=utf-8');
$app->get('/', function () {
    echo "SlimTarefas";
});
$app->get('/conta','multiplos');
$app->post('/tarefas', 'addTarefas');
$app->get('/tarefas/:id', 'getTarefa');
$app->post('/tarefas/:id', 'saveTarefas');
$app->delete('/tarefas/:id', 'deleteTarefas');
$app->get('/tarefas', 'getTarefas');
$app->run();


function multiplos()
{
    for ($index = 0; $index < 100; $index++) {
       if ($index % 3 == 0){
         echo $index . " FIZZ " . PHP_EOL;
       } elseif ( $index % 5 == 0) {
            echo $index . " BUZZ " . PHP_EOL;
       } else {
           echo $index . " FIZZBUZZ " . PHP_EOL;
       }
    }
}

function addTarefas() 
{

    $app = \Slim\Slim::getInstance();
    $request = $app->request();
    $titulo = $request->post('titulo');
    $descricao = $request->post('descricao');
    $prioridade = $request->post('prioridade');
    $tarefa = array('titulo' => $titulo, 'descricao' => $descricao, 'prioridade' => $prioridade);
    $sql = "INSERT INTO tarefas (titulo,descricao, prioridade) values ('$titulo','$descricao', '$prioridade') ";
    $conn = getConn();
    $stmt = $conn->prepare($sql);
    $stmt->bindParam("titulo", $tarefa['titulo']);
    $stmt->bindParam("descricao", $tarefa['descricao']);
    $stmt->execute();
    //$tarefa->id = $conn->lastInsertId();
    echo json_encode($tarefa);
}

function getConn() 
{
    return new PDO('mysql:host=localhost;dbname=teste', 'root', 'admin', array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8")
    );
}

function getTarefasAll() 
{
    $stmt = getConn()->query("SELECT * FROM tarefas");
    $tarefas = $stmt->fetchAll(PDO::FETCH_OBJ);
    echo "{tarefas:" . json_encode($tarefas) . "}";
}

function getTarefa($id) 
{
    $conn = getConn();
    $sql = "SELECT * FROM tarefas WHERE id=:id";
    $stmt = $conn->prepare($sql);
    $stmt->bindParam("id", $id);
    $stmt->execute();
    $tarefa = $stmt->fetchObject();

    echo json_encode($tarefa);
}

function saveTarefas($id) 
{
    $app = \Slim\Slim::getInstance();
    $request = $app->request();
    $titulo = $request->post('titulo');
    $descricao = $request->post('descricao');
    $prioridade = $request->post('prioridade');
    $tarefa = array('titulo' => $titulo, 'descricao' => $descricao, 'prioridade' => $prioridade);
    $sql = "UPDATE tarefas SET titulo='$titulo',descricao='$descricao', prioridade = '$prioridade' WHERE   id='$id'";
    $conn = getConn();
    $stmt = $conn->prepare($sql);
    $stmt->bindParam("titulo", $titulo);
    $stmt->bindParam("descricao", $descricao);
    $stmt->bindParam("id", $id);
    $stmt->execute();

    echo json_encode($tarefa);
}

function deleteTarefas($id) 
{
    $sql = "DELETE FROM tarefas WHERE id=:id";
    $conn = getConn();
    $stmt = $conn->prepare($sql);
    $stmt->bindParam("id", $id);
    $stmt->execute();
    echo "{'message':'Tarefa apagada'}";
}

function getTarefas() 
{
    $sql = "SELECT * FROM tarefas ORDER BY prioridade DESC";
    $stmt = getConn()->query($sql);
    $tarefas = $stmt->fetchAll(PDO::FETCH_OBJ);
    echo "{\"tarefas\":" . json_encode($tarefas) . "}";
}
